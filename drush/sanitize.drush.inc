<?php

/**
 * @file
 * Drush commands for Sanitize module.
 */

/**
 * Implements hook_drush_sql_sync_sanitize().
 */use Drupal\sanitize\Sanitizer;

/**
 * Implements hook_drush_sql_sync_sanitize().
 */
function sanitize_drush_sql_sync_sanitize($site) {
  require __DIR__ . '/../src/Sanitizer.php';
  $sanitizer = new Sanitizer($site);
  $sanitizer->sanitize();
}
