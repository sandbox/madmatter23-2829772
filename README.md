# Sanitize

This module extends the `drush sql-sanitize` and `drush sql-sync --sanitize` commands to sanitize additional database tables provided by Drupal core.

This module will never sanitize tables provided by contributed modules because that sanitization should be performed by said contributed modules. See [hook_drush_sql_sync_sanitize()](http://api.drush.org/api/drush/drush.api.php/function/hook_drush_sql_sync_sanitize/8.0.x) for implementation information.

## Usage

Execute `drush sql-sanitize`. For more information on the command, execute `drush sql-sanitize --help`.
